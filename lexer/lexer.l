%class-name = "TigerScanner"
%baseclass-header = "scannerbase.hpp"
%class-header = "scanner.hpp"
%implementation-header = "scannerinc.hpp"
%lex-source = "lex.cpp"

// define exclusives start states for comments and strings
%x comment string span


%%

// keep column index updated
<*>[[:space:]]{-}[\n]+      d_column += length();
<string>\n+                 {
                                accept();
                                leave(1);
                            }
<*>\n+                      d_column = 0;
/* put keywords before identifiers to prevent
   identifiers from naming keywords */
"array"                     return TigerParser::ARRAY;
"break"                     return TigerParser::BREAK;
"do"                        return TigerParser::DO;
"else"                      return TigerParser::ELSE;
"end"                       return TigerParser::END;
"for"                       return TigerParser::FOR;
"function"                  return TigerParser::FUNCTION;
"if"                        return TigerParser::IF;
"in"                        return TigerParser::IN;
"let"                       return TigerParser::LET;
"nil"                       return TigerParser::NIL;
"of"                        return TigerParser::OF;
"then"                      return TigerParser::THEN;
"to"                        return TigerParser::TO;
"type"                      return TigerParser::TYPE;
"var"                       return TigerParser::VAR;
"while"                     return TigerParser::WHILE;
[[:alpha:]_][[:alnum:]_]*   return TigerParser::IDENTIFIER;
[[:digit:]]+                return TigerParser::INTEGER;
","                         return TigerParser::COMMA;
":"                         return TigerParser::COLON;
";"                         return TigerParser::SEMI_COLON;
"("                         return TigerParser::LEFT_PAREN;
")"                         return TigerParser::RIGHT_PAREN;
"["                         return TigerParser::LEFT_BRACK;
"]"                         return TigerParser::RIGHT_BRACK;
"{"                         return TigerParser::LEFT_BRACE;
"}"                         return TigerParser::RIGHT_BRACE;
"."                         return TigerParser::DOT;
"+"                         return TigerParser::PLUS;
"-"                         return TigerParser::MINUS;
"*"                         return TigerParser::MULT;
"/"                         return TigerParser::DIV;
"="                         return TigerParser::EQUAL;
"<>"                        return TigerParser::CMP;
"<"                         return TigerParser::CMP;
"<="                        return TigerParser::CMP;
">"                         return TigerParser::CMP;
">="                        return TigerParser::CMP;
"&"                         return TigerParser::AND;
"|"                         return TigerParser::OR;
":="                        return TigerParser::ASSIGN;
// switch to comment state
<INITIAL,comment>"/*"       {
                                begin(StartCondition__::comment);
                                d_comment_nest++;
                            }
// switch to initial state depending on nesting level
<comment> {
    "*/"                    {
                                d_comment_nest--;
                                if (not d_comment_nest)
                                    begin(StartCondition__::INITIAL);
                            }
    <<EOF>>                 leave(1);  // comment not ended
    .
}
\"                          {
                                // start matching a string
                                begin(StartCondition__::string);
                            }
// string read condition patterns
<string> {
    \"                      {
                                // end of string condition
                                begin(StartCondition__::INITIAL);
                                setMatched(d_pendingStr.append(matched(), 0, length() - 1));
                                d_pendingStr.clear();
                                return TigerParser::STRING;
                            }
    // string scape
    \\[nt\"\\]              |
    \\\^[@A-Z\\\^_]         |
    \\[[:digit:]]{3}        more();  // append matched text to next match

    // save text before a line span
    \\                      {
                                d_pendingStr.append(matched(), 0, length() - 1);
                                begin(StartCondition__::span);
                            }
    <<EOF>>                 leave(1);  // string not ended
    .                       more();  // keep appending text
}
<span> {
    \\                      begin(StartCondition__::string);
    <<EOF>>                 leave(1);  // string span not ended
    .                       {
                                setMatched('\\' + matched());
                                d_column += d_pendingStr.length() + 2;
                                leave(1);  // non-whitespace character found in span
                            }
}
