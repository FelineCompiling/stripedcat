#include <iostream>
#include <fstream>
#include <cerrno>

#include <parser/parser.hpp>

using namespace std;

int main(int argc,  const char *argv[]) {
    cout << "StripedCat version " PROJECT_VERSION << endl;
    cout << "Copyright (C) 2016-2017 Annie Hernández & Rolando Urquiza"
         << endl << endl;

    int exit_code = 0;
    if (argc > 1) {
        ifstream fs(argv[1], ios_base::in);  // open file as stream
        // if the file doesn't exist, `open` sets errno to ENOENT
        if (not fs and errno == ENOENT) {
            printf("(0, 0): File '%s' couldn't be found.\n", argv[1]);
            exit_code = 1;
        }
        else {
            TigerParser parser(fs);
            // use parser success return as exit code
            exit_code = parser.parse();
        }
    }

    if (exit_code)
        cout << endl;
    return exit_code;
}
