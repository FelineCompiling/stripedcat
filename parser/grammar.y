%scanner "../lexer/scanner.hpp"
%scanner-class-name TigerScanner
%class-header parser.hpp
%class-name TigerParser
%implementation-header parserinc.hpp
%baseclass-header parserbase.hpp
%baseclass-preinclude astinclude.hpp
%parsefun-source parse.cpp

%lsp-needed
%locationstruct {
  int timestamp;
  int first_line;
  int first_column;
  int last_line;
  int last_column;
  const std::string *text;
};

// lowest precedent
%token ARRAY BREAK DO END FOR IF IN LET NIL OF THEN TO
       VAR WHILE IDENTIFIER INTEGER COMMA COLON SEMI_COLON LEFT_PAREN
       RIGHT_PAREN LEFT_BRACK RIGHT_BRACK LEFT_BRACE RIGHT_BRACE DOT
       ASSIGN STRING
%left FUNCTION TYPE ELSE
%left OR
%left AND
%nonassoc CMP EQUAL
%left PLUS MINUS
%left MULT DIV
%right UNARY
// highest precedent

// fields for semantic type declaraation
%polymorphic
  EXPR:             ast::expression::ExprASTNode*;
  EXPRSEQ:          ast::expression::ExprSeqASTNode*;
  EXPRLST:          std::vector<ast::expression::ExprASTNode*>*;
  DECL:             ast::declaration::DeclarationASTNode*;
  DECLSEQ:          std::vector<ast::declaration::DeclarationASTNode*>*;
  VARDECL:          ast::declaration::VarDeclASTNode*;
  FUNCDECL:         ast::declaration::FuncDeclASTNode*;
  FUNCDECLSEQ:      ast::declaration::FuncDeclSeqASTNode*;
  TYPEDECL:         ast::declaration::TypeDeclASTNode*;
  TYPEDECLSEQ:      ast::declaration::TypeDeclSeqASTNode*;
  TYPEFIELD:        ast::util::TypeField*;
  TYPEFIELDLST:     std::vector<ast::util::TypeField*>*;
  FIELDLST:         std::vector<ast::util::RecordField*>*;
  LVALUE:           ast::leftvalue::LvalASTNode*;
  CHAR:             char;
  STR:              std::string;
  // tags required by polymorphic
  BINEXPR:          ast::expression::BinaryASTNode*;
  NEXPR:            ast::expression::NegASTNode*;
  VARIABLE:         ast::leftvalue::LVariableASTNode*;
  ASSIGN:           ast::statement::AssignASTNode*;
  IFTHEN:           ast::statement::IfThenASTNode*;
  IFTHENELSE:       ast::expression::IfThenElseASTNode*;
  WHILE:            ast::statement::WhileASTNode*;
  FOR:              ast::statement::ForASTNode*;
  BREAK:            ast::statement::BreakASTNode*;
  LETINEND:         ast::expression::LetInEndASTNode*;
  STRING:           ast::expression::StringASTNode*;
  INT:              ast::expression::IntASTNode*;
  NIL:              ast::expression::NilASTNode*;
  FUNCALL:          ast::expression::FuncCallASTNode*;
  RECORDCRT:        ast::expression::RecordCreatASTNode*;
  ARRAYCRT:         ast::expression::ArrayCreatASTNode*;
  DOTACCESS:        ast::leftvalue::DotAccessASTNode*;
  ARRACCESS:        ast::leftvalue::ArrayAccessASTNode*;
  ALIASTYPEDECL:    ast::declaration::AliasTypeDeclASTNode*;
  RECORDTYPEDECL:   ast::declaration::RecordTypeDeclASTNode*;
  ARRAYTYPEDECL:    ast::declaration::ArrayTypeDeclASTNode*;

// type of each non-terminal
%type <EXPR> expr operator id_aux
%type <EXPRSEQ> expr_seq
%type <EXPRLST> expr_list
%type <FIELDLST> field_list
%type <LVALUE> lvalue lvalue_aux

%type <DECL> declaration
%type <DECLSEQ> declaration_seq
%type <VARDECL> variable_declaration
%type <FUNCDECL> function_declaration
%type <FUNCDECLSEQ> function_sequence
%type <TYPEDECL> type_declaration type
%type <TYPEDECLSEQ> type_sequence
%type <TYPEFIELD> type_field
%type <TYPEFIELDLST> type_fields

%type <CHAR> CMP
%type <STR> id type_id

%%

program: expr EOF { d_program = $1; };

// the left, nonassoc and right sentences handles the conflicts
expr: expr AND expr
      {
        $$(new ast::expression::BinaryASTNode('&', $1, $3));
      }
    | expr OR expr
      {
        $$(new ast::expression::BinaryASTNode('|', $1, $3));
      }
    | expr PLUS expr
      {
        $$(new ast::expression::BinaryASTNode('+', $1, $3));
      }
    | expr MINUS expr
      {
        $$(new ast::expression::BinaryASTNode('-', $1, $3));
      }
    | expr MULT expr
      {
        $$(new ast::expression::BinaryASTNode('*', $1, $3));
      }
    | expr DIV expr
      {
        $$(new ast::expression::BinaryASTNode('/', $1, $3));
      }
    | expr CMP { $2 = d_scanner.matched()[0]; } expr
      {
        $$(new ast::expression::BinaryASTNode($2, $1, $4));
      }
    | expr EQUAL expr
      {
        $$(new ast::expression::BinaryASTNode('=', $1, $3));
      }
    // most significant operation
    | MINUS expr %prec UNARY
      {
        $$(new ast::expression::NegASTNode($2));
      }
    | operator
      {
        $$ = $1;
      }
;

// separate operable part from expr
operator: id
          {
            $$(new ast::leftvalue::LVariableASTNode(getInfo<ast::inspect::VariableInfo>(d_scanner.matched())));
          }
        | id id_aux { $$ = $2; }
        | lvalue_aux { $$ = $1; }
        | lvalue ASSIGN expr
          {
            $$(new ast::statement::AssignASTNode($1, $3));
          }
        | LEFT_PAREN expr_seq RIGHT_PAREN { $$ = $2; }
        | LEFT_PAREN RIGHT_PAREN { $$ = &ast::expression::EmptySeq; }
        | IF expr THEN expr
          {
            $$(new ast::statement::IfThenASTNode($2, $4));
          }
        | IF expr THEN expr ELSE expr
          {
            $$(new ast::expression::IfThenElseASTNode($2, $4, $6));
          }
        | WHILE expr DO expr
          {
            $$(new ast::statement::WhileASTNode($2, $4));
          }
        | FOR id ASSIGN expr TO expr DO expr
          {
            auto var = new ast::declaration::VarDeclASTNode($2, $4);
            $$(new ast::statement::ForASTNode(var, $6, $8));
          }
        | BREAK { $$ = new ast::statement::BreakASTNode(); }
        | LET declaration_seq IN expr_seq END
          {
            $$(new ast::expression::LetInEndASTNode($2, $4));
          }
        | LET declaration_seq IN END
          {
            $$(new ast::expression::LetInEndASTNode($2, &ast::expression::EmptySeq));
          }
        | STRING { $$(new ast::expression::StringASTNode(d_scanner.matched())); }
        | INTEGER { $$(new ast::expression::IntASTNode(std::stoi(d_scanner.matched()))); }
        | NIL { $$(&ast::expression::Nil); }
;

id: IDENTIFIER { $$ = d_scanner.matched(); }
;

id_aux: LEFT_PAREN expr_list RIGHT_PAREN
        {
          const ast::inspect::FunctionInfo *f = getInfo<ast::inspect::FunctionInfo>($<STR>-1);
          $$(new ast::expression::FuncCallASTNode(f, $2));
        }
      | LEFT_PAREN RIGHT_PAREN
        {
          const ast::inspect::FunctionInfo *f = getInfo<ast::inspect::FunctionInfo>($<STR>-1);
          $$(new ast::expression::FuncCallASTNode(f, nullptr));
        }
      | LEFT_BRACE field_list RIGHT_BRACE
        {
          const ast::inspect::RecordTypeInfo *t = getInfo<ast::inspect::RecordTypeInfo>($<STR>-1);
          $$(new ast::expression::RecordCreatASTNode(t, $2));
        }
      | LEFT_BRACE RIGHT_BRACE
        {
          const ast::inspect::RecordTypeInfo *t = getInfo<ast::inspect::RecordTypeInfo>($<STR>-1);
          $$(new ast::expression::RecordCreatASTNode(t, {}));
        }
      | LEFT_BRACK expr RIGHT_BRACK OF expr
        {
          const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($<STR>-1);
          $$(new ast::expression::ArrayCreatASTNode(t, $2, $5));
        }
;

lvalue:  id { $$(new ast::leftvalue::LVariableASTNode(getInfo<ast::inspect::VariableInfo>($1))); }
      | lvalue_aux { $$ = $1; }
;

lvalue_aux: lvalue DOT id
            {
              auto type = static_cast<const ast::inspect::RecordTypeInfo*>($1->returns);

              bool found = false;
              for (ast::util::TypeField *f: *type)
                if (f->name == $3)
                {
                  auto field = new ast::leftvalue::FieldAccessASTNode(f);
                  $$(new ast::leftvalue::DotAccessASTNode($1, field));
                  found = true;
                  break;
                }

              if (not found)
                ERROR();
            }
          | lvalue_aux LEFT_BRACK expr RIGHT_BRACK
            {
              $$(new ast::leftvalue::ArrayAccessASTNode($1, $3));
            }
          | id LEFT_BRACK expr RIGHT_BRACK
            {
              auto var = new ast::leftvalue::LVariableASTNode(getInfo<ast::inspect::VariableInfo>($1));
              $$(new ast::leftvalue::ArrayAccessASTNode(var, $3));
            }
;

expr_seq: expr_seq SEMI_COLON expr
          {
            $$ = $1;
            $$->insert($$->begin(), $3);
          }
        | expr { $$ = new ast::expression::ExprSeqASTNode({$1}); }
;

expr_list: expr_list COMMA expr
          {
            $$ = $1;
            $$->insert($$->begin(), $3);
          }
         | expr { $$ = new std::vector<ast::expression::ExprASTNode*>{$1}; }
;

field_list: field_list COMMA id EQUAL expr
            {
              $$ = $1;
              auto field = new ast::util::RecordField($3, $5);
              $$->push_back(field);
            }
          | id EQUAL expr
            {
              auto field = new ast::util::RecordField($1, $3);
              $$ = new std::vector<ast::util::RecordField*>{field};
            }
;


declaration_seq: declaration_seq declaration
                {
                  $$ = $1;
                  $$->insert($$->begin(), $2);
                }
                | declaration { $$ = new std::vector<ast::declaration::DeclarationASTNode*>{$1}; }
;

// create declaration sequences to control scope
declaration: FUNCTION id LEFT_PAREN function_sequence { $$($4); }
           | TYPE type_sequence {$$($2);}
           | VAR id variable_declaration {$$($3);}
;

variable_declaration: ASSIGN expr
                      {
                        $$ = new ast::declaration::VarDeclASTNode($<STR>-1, $2);
                        d_scope.insert({$<STR>-1, $$->getInfo()});
                      }
                    | COLON type_id ASSIGN expr
                      {
                        const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($2);
                        $$ = new ast::declaration::VarDeclASTNode($<STR>-1, t, $4);
                        d_scope.insert({$<STR>-1, $$->getInfo()});
                      }
;

function_sequence: function_sequence FUNCTION id LEFT_PAREN function_declaration
                   {
                      $$ = $1;
                      $$->insert($$->begin(), $5);
                   }
                 | function_declaration { $$ = new ast::declaration::FuncDeclSeqASTNode{$1}; }
;

function_declaration: type_fields RIGHT_PAREN EQUAL expr
                      {
                        $$ = new ast::declaration::FuncDeclASTNode($<STR>-2, $1, $4);
                        d_scope.insert({$<STR>-2, $$->getInfo()});
                      }
                    | RIGHT_PAREN EQUAL expr
                      {
                        $$ = new ast::declaration::FuncDeclASTNode($<STR>-2, {}, $3);
                        d_scope.insert({$<STR>-2, $$->getInfo()});
                      }
                    | type_fields RIGHT_PAREN COLON type_id EQUAL expr
                      {
                        const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($4);
                        $$ = new ast::declaration::FuncDeclASTNode($<STR>-2, $1, t, $6);
                        d_scope.insert({$<STR>-2, $$->getInfo()});
                      }
                    | RIGHT_PAREN COLON type_id EQUAL expr
                      {
                        const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($3);
                        $$ = new ast::declaration::FuncDeclASTNode($<STR>-2, {}, t, $5);
                        d_scope.insert({$<STR>-2, $$->getInfo()});
                      }
;

type_sequence: type_sequence TYPE type_declaration
               {
                  $$ = $1;
                  $$->insert($$->begin(), $3);
               }
             | type_declaration { $$ = new ast::declaration::TypeDeclSeqASTNode{$1}; }
;

type_declaration: type_id EQUAL type { $$ = $3; }
;

type: type_id
      {
        const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($1);
        $$(new ast::declaration::AliasTypeDeclASTNode($<STR>-2, t));
        d_scope.insert({$<STR>-2, $$->getInfo()});
      }
    | LEFT_BRACE type_fields RIGHT_BRACE
      {
        $$(new ast::declaration::RecordTypeDeclASTNode($<STR>-2, $2));
        d_scope.insert({$<STR>-2, $$->getInfo()});
      }
    | LEFT_BRACE RIGHT_BRACE
      {
        $$(new ast::declaration::RecordTypeDeclASTNode($<STR>-2, {}));
        d_scope.insert({$<STR>-2, $$->getInfo()});
      }
    | ARRAY OF type_id
      {
        const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($3);
        $$(new ast::declaration::ArrayTypeDeclASTNode($<STR>-2, t));
        d_scope.insert({$<STR>-2, $$->getInfo()});
      }
;

type_fields: type_fields COMMA type_field
             {
                $$ = $1;
                $$->insert($$->begin(), $3);
             }
           | type_field { $$ = new std::vector<ast::util::TypeField*>{$1}; }
;

type_field: id COLON type_id
            {
              const ast::inspect::TypeInfo *t = getInfo<ast::inspect::TypeInfo>($3);
              $$ = new ast::util::TypeField($1, t);
            }
;

type_id: IDENTIFIER { $$ = d_scanner.matched(); }
;
