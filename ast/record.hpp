#ifndef _RECORD_AST_H_
#define _RECORD_AST_H_

#include "base.hpp"
#include "type.hpp"


namespace ast
{
    namespace inspect
    {
        class RecordTypeInfo : public TypeInfo
        {
            const std::vector<util::TypeField*> *d_fields;
        public:
            RecordTypeInfo(const std::string &name,
                           const std::vector<util::TypeField*> *fields)
            :
                TypeInfo(name), d_fields(fields)
            {}

            std::vector<util::TypeField*>::const_iterator begin(void) const
            {
                return d_fields->begin();
            }

            std::vector<util::TypeField*>::const_iterator end(void) const
            {
                return d_fields->end();
            }
        };

    }

    namespace declaration
    {
        class RecordTypeDeclASTNode : public TypeDeclASTNode
        {
            inspect::RecordTypeInfo *d_type;
        public:
            RecordTypeDeclASTNode(const std::string &name,
                                  const std::vector<util::TypeField*> *fields)
            :
                d_type(new inspect::RecordTypeInfo(name, fields))
            {}

            const inspect::RecordTypeInfo* getInfo(void) override
            {
                return d_type;
            }
        };
    }

    namespace util
    {
        struct RecordField
        {
            const std::string name;
            expression::ExprASTNode *value;
            RecordField(const std::string &name,
                        expression::ExprASTNode *value)
            :
                name(name), value(value)
            {}
        };
    }

    namespace expression
    {
        class RecordCreatASTNode : public ExprASTNode
        {
            const inspect::TypeInfo *d_type;
            const std::vector<util::RecordField*> *d_fields;
        public:
            RecordCreatASTNode(const inspect::TypeInfo *type,
                               const std::vector<util::RecordField*> *fields)
            :
                d_type(type), d_fields(fields), ExprASTNode(type)
            {}
        };
    }

    namespace leftvalue
    {
        class DotAccessASTNode : public LvalASTNode
        {
            LvalASTNode *d_left, *d_right;
        public:
            DotAccessASTNode(LvalASTNode *left, LvalASTNode *right)
            :
                d_left(left), d_right(right), LvalASTNode(right->returns)
            {}
        };


        class FieldAccessASTNode : public LvalASTNode
        {
            util::TypeField *d_field;
        public:
            FieldAccessASTNode(util::TypeField *field)
            :
                d_field(field), LvalASTNode(field->type)
            {}
        };
    }
}

#endif
