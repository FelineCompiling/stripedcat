#ifndef _BASE_AST_H_
#define _BASE_AST_H_

#include <iostream>
#include <vector>
#include <memory>


/*
 * base AST nodes
 */

namespace ast
{
    class BaseASTNode
    {
    public:
        virtual ~BaseASTNode() {}
    };


    namespace inspect
    {
        struct Info
        {
            const std::string name;
            Info(const std::string &name)
            :
                name(name)
            {}
        };


        struct TypeInfo : public Info
        {
            using Info::Info;
        };

        static TypeInfo String("string");
        static TypeInfo Numeric("int");
        static TypeInfo Nil("nil");
        static TypeInfo None("");  // created for statements
    }

    namespace declaration
    {
        class DeclarationASTNode : public BaseASTNode
        {
        public:
            virtual const inspect::Info* getInfo(void) {}
        };


        class TypeDeclASTNode : public DeclarationASTNode
        {
        public:
            virtual const inspect::TypeInfo* getInfo(void) override {}
        };
    }

    namespace expression
    {
        using namespace inspect;

        class ExprASTNode : public BaseASTNode
        {
        public:
            const TypeInfo *returns;  // return value type
            ExprASTNode(const TypeInfo *returns)
            :
                returns(returns)
            {}
        };
    }

    namespace statement
    {
        using namespace expression;

        class StatementASTNode : public ExprASTNode
        {
        public:
            StatementASTNode(void);
        };

        inline StatementASTNode::StatementASTNode(void)
        :
            ExprASTNode(&inspect::None)
        {}
    }

    namespace leftvalue
    {
        class LvalASTNode : public expression::ExprASTNode
        {
        public:
            using ExprASTNode::ExprASTNode;
        };
    }
}

#endif
