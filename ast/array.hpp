#ifndef _ARRAY_AST_H_
#define _ARRAY_AST_H_

#include "base.hpp"
#include "record.hpp"
#include "type.hpp"


namespace ast
{
    namespace expression
    {
        class ArrayCreatASTNode : public ExprASTNode
        {
            const inspect::TypeInfo *d_type;
            ExprASTNode *d_size, *d_initializer;
        public:
            ArrayCreatASTNode(const inspect::TypeInfo *type, ExprASTNode *size, ExprASTNode *initializer)
            :
                d_type(type), d_size(size), d_initializer(initializer), ExprASTNode(type)
            {}
        };
    }

    namespace inspect
    {
        struct ArrayTypeInfo : public TypeInfo
        {
            const TypeInfo *type;
            ArrayTypeInfo(const std::string& name, const TypeInfo *type)
            :
                type(type), TypeInfo(name)
            {}
        };
    }

    namespace declaration
    {
        class ArrayTypeDeclASTNode : public TypeDeclASTNode
        {
            inspect::ArrayTypeInfo *d_type;
        public:
            ArrayTypeDeclASTNode(const std::string &name, const inspect::TypeInfo *source)
            :
                d_type(new inspect::ArrayTypeInfo(name, source))
            {}

            const inspect::ArrayTypeInfo* getInfo(void) override
            {
                return d_type;
            }
        };
    }

    namespace leftvalue
    {
        class ArrayAccessASTNode : public LvalASTNode
        {
            LvalASTNode *d_field;
            ExprASTNode *d_index;
        public:
            ArrayAccessASTNode(LvalASTNode *field, ExprASTNode *index)
            :
                d_field(field), d_index(index), LvalASTNode(field->returns)
            {}
        };
    }
}

#endif
