#ifndef _TYPE_H_
#define _TYPE_H_

#include "base.hpp"


namespace ast
{
    namespace inspect
    {
        struct AliasTypeInfo : public TypeInfo
        {
            const TypeInfo *alias;
            AliasTypeInfo(const std::string& name, const TypeInfo *alias)
            :
                alias(alias), TypeInfo(name)
            {}
        };
    }

    namespace declaration
    {
        class AliasTypeDeclASTNode : public TypeDeclASTNode
        {
            inspect::AliasTypeInfo *d_type;
        public:
            AliasTypeDeclASTNode(const std::string &name, const inspect::TypeInfo *source)
            :
                d_type(new inspect::AliasTypeInfo(name, source))
            {}

            const inspect::AliasTypeInfo* getInfo(void) override
            {
                return d_type;
            }
        };


        class TypeDeclSeqASTNode : public TypeDeclASTNode, public std::vector<TypeDeclASTNode*>
        {
        public:
            using std::vector<TypeDeclASTNode*>::vector;
        };
    }

    namespace util
    {
        struct TypeField
        {
            const std::string name;
            const inspect::TypeInfo *type;
            TypeField(const std::string &name, const inspect::TypeInfo *type)
            :
                name(name), type(type)
            {}
        };
    }
}

#endif
