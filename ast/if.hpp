#ifndef _IF_AST_H_
#define _IF_AST_H_

#include "base.hpp"


namespace ast
{
    namespace statement
    {
        class IfThenASTNode : public StatementASTNode
        {
            expression::ExprASTNode *d_ifexpr, *d_thenexpr;

        public:
            IfThenASTNode(expression::ExprASTNode *ifexpr,
                          expression::ExprASTNode *thenexpr)
            :
                d_ifexpr(ifexpr), d_thenexpr(thenexpr)
            {}
        };
    }

    namespace expression
    {
        class IfThenElseASTNode : public ExprASTNode
        {
            ExprASTNode *d_ifexpr, *d_thenexpr, *d_elsexpr;

        public:
            IfThenElseASTNode(ExprASTNode *ifexpr, ExprASTNode *thenexpr, ExprASTNode *elsexpr)
            :
                d_ifexpr(ifexpr), d_thenexpr(thenexpr), d_elsexpr(elsexpr),
                ExprASTNode(thenexpr->returns)
            {}
        };
    }
}

#endif
