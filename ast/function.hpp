#ifndef _FUNCTION_AST_H_
#define _FUNCTION_AST_H_

#include "base.hpp"
#include "type.hpp"


namespace ast
{
    namespace inspect
    {
        struct FunctionInfo : public Info
        {
            const TypeInfo *returns;
            const std::vector<util::TypeField*> *params;
            FunctionInfo(const std::string &name, const TypeInfo *returns,
                         const std::vector<util::TypeField*> *params)
            :
                returns(returns), params(params), Info(name)
            {}

            // void function
            FunctionInfo(const std::string &name, const std::vector<util::TypeField*> *params)
            :
                FunctionInfo(name, &None, params)
            {}
        };
    }

    namespace declaration
    {
        class FuncDeclASTNode : public DeclarationASTNode
        {
            inspect::FunctionInfo *d_function;
            expression::ExprASTNode *d_body;

        public:
            FuncDeclASTNode(const std::string &name, const std::vector<util::TypeField*> *params,
                            const inspect::TypeInfo *returns, expression::ExprASTNode *body)
            :
                d_function(new inspect::FunctionInfo(name, returns, params)), d_body(body)
            {}

            FuncDeclASTNode(const std::string &name, const std::vector<util::TypeField*> *params,
                            expression::ExprASTNode *body)
            :
                d_function(new inspect::FunctionInfo(name, params)), d_body(body)
            {}

            const inspect::FunctionInfo* getInfo(void)
            {
                return d_function;
            }
        };


        class FuncDeclSeqASTNode : public DeclarationASTNode, public std::vector<FuncDeclASTNode*>
        {
        public:
            using std::vector<FuncDeclASTNode*>::vector;
        };
    }

    namespace expression
    {
        class FuncCallASTNode : public ExprASTNode
        {
            const inspect::FunctionInfo *d_function;
            const std::vector<ExprASTNode*> *d_params;

        public:
            FuncCallASTNode(const inspect::FunctionInfo *function,
                            const std::vector<ExprASTNode*> *params)
            :
                d_function(function), d_params(params), ExprASTNode(function->returns)
            {}
        };
    }
}

#endif
