#ifndef _VARIABLE_AST_H_
#define _VARIABLE_AST_H_

#include "base.hpp"


namespace ast
{
    namespace inspect
    {
        struct VariableInfo : public Info
        {
            const TypeInfo *type;
            VariableInfo(const std::string &name, const TypeInfo *type)
            :
                type(type), Info(name)
            {}
        };
    }

    namespace declaration
    {
        class VarDeclASTNode : public DeclarationASTNode
        {
            const inspect::VariableInfo *d_variable;
            expression::ExprASTNode *d_expr;
        public:
            VarDeclASTNode(const std::string &name, const inspect::TypeInfo *type,
                           expression::ExprASTNode *expr)
            :
                d_variable(new inspect::VariableInfo(name, type)), d_expr(expr)
            {}

            VarDeclASTNode(const std::string &name, expression::ExprASTNode *expr)
            :
                d_expr(expr), d_variable(new inspect::VariableInfo(name, expr->returns))
            {}

            const inspect::VariableInfo* getInfo(void)
            {
                return d_variable;
            }
        };
    }

    namespace statement
    {
        class AssignASTNode : public StatementASTNode
        {
            expression::ExprASTNode *d_value;
            leftvalue::LvalASTNode *d_holder;

        public:
            AssignASTNode(leftvalue::LvalASTNode *holder,
                          expression::ExprASTNode *value)
            :
                d_holder(holder), d_value(value)
            {}
        };
    }

    namespace leftvalue
    {
        class LVariableASTNode : public LvalASTNode
        {
            const inspect::VariableInfo *d_variable;
        public:
            LVariableASTNode(const inspect::VariableInfo *variable)
            :
                d_variable(variable), LvalASTNode(variable->type)
            {}
        };
    }
}

#endif
