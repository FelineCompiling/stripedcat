#ifndef _LOOP_AST_H_
#define _LOOP_AST_H_

#include "base.hpp"
#include "variable.hpp"


namespace ast
{
    namespace statement
    {
        using namespace expression;

        class BreakASTNode : public StatementASTNode {};


        class ForASTNode : public StatementASTNode
        {
            declaration::VarDeclASTNode *d_loopvar;
            ExprASTNode *d_end, *d_body;

        public:
            ForASTNode(declaration::VarDeclASTNode *loopvar,
                       ExprASTNode *end,
                       ExprASTNode *body)
            :
                d_loopvar(loopvar), d_end(end), d_body(body)
            {}
        };


        class WhileASTNode : public StatementASTNode
        {
            ExprASTNode *d_condition, *d_body;

        public:
            WhileASTNode(ExprASTNode *condition,
                         ExprASTNode *body)
            :
                d_condition(condition), d_body(body)
            {}
        };
    }
}

#endif
