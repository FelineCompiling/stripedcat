#ifndef _EXPR_AST_H_
#define _EXPR_AST_H_

#include "base.hpp"


namespace ast
{
	namespace expression
	{
		class ExprSeqASTNode : public ExprASTNode, public std::vector<ExprASTNode*>
		{
		public:
			ExprSeqASTNode(std::vector<ExprASTNode*> &&exprs)
			:
				ExprASTNode(exprs.back()->returns), std::vector<ExprASTNode*>(exprs)
			{}

			ExprSeqASTNode(TypeInfo *returns)
			:
				std::vector<ExprASTNode*>{}, ExprASTNode(returns)
			{}
		};


		static ExprSeqASTNode EmptySeq(&inspect::None);


		class LetInEndASTNode : public ExprASTNode
		{
			const std::vector<declaration::DeclarationASTNode*> *d_decls;
			ExprSeqASTNode *d_exprs;

		public:
			LetInEndASTNode(std::vector<declaration::DeclarationASTNode*> *decls,
				   			ExprSeqASTNode* exprs)
			:
				d_decls(decls), d_exprs(exprs), ExprASTNode(exprs->returns)
			{}
		};


		class NegASTNode : public ExprASTNode
		{
		    ExprASTNode *d_operand;
		public:
			NegASTNode(ExprASTNode* operand)
			:
			    d_operand(operand), ExprASTNode(operand->returns)
			{}
		};


		class BinaryASTNode : public ExprASTNode
		{
		    char d_opcode;
		    ExprASTNode *d_left, *d_right;
		public:
			BinaryASTNode(char opcode, ExprASTNode *left, ExprASTNode *right)
			:
			    d_opcode(opcode), d_left(left), d_right(right), ExprASTNode(left->returns)
			{}
		};


		class StringASTNode : public ExprASTNode
		{
			const std::string d_value;
		public:
			StringASTNode(const std::string &value)
			:
				d_value(value), ExprASTNode(&inspect::String)
			{}
		};


		class IntASTNode : public ExprASTNode
		{
			int d_value;
		public:
			IntASTNode(int value)
			:
				d_value(value), ExprASTNode(&inspect::Numeric)
			{}
		};


		class NilASTNode : public ExprASTNode
		{
		public:
			NilASTNode(void)
			:
				ExprASTNode(&inspect::Nil)
			{}
		};

		static NilASTNode Nil;
    }
}

#endif
